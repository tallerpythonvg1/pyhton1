from django import forms
from django.contrib import admin
from .models import Registrado

class RegForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    email = forms.EmailField()

class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = Registrado
        campos = ["nombre","email"]

    def clean_email(self):
        email = self.cleaned_data.get("email") #campturamos email
        email_base, proveedor = email.split("@") #separamos hasta el @
        dominio, extencion = proveedor.split(".") #separamos hasta la extencion
        if not extencion == "edu": # validamos que sea .edu
            raise forms.ValidationError("Solo se permite extension. EDU")
        return email